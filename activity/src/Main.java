import java.util.Scanner;
import java.util.ArrayList;
import java.util.HashMap;

public class Main {
        public static void main(String[] args) {
            //Prime numbers

            int[] primes = new int[5];

            primes[0] = 2;
            primes[1] = 3;
            primes[2] = 5;
            primes[3] = 7;
            primes[4] = 11;


            System.out.println("The first prime number is: " + primes[0]);
            System.out.println("The second prime number is: " + primes[1]);
            System.out.println("The third prime number is: " + primes[2]);
            System.out.println("The fourth prime number is: " + primes[3]);
            System.out.println("The fifth prime number is: " + primes[4]);


            // Leap year
            Scanner input = new Scanner(System.in);


            System.out.println("Enter a year: ");
            int year = input.nextInt();


            boolean isLeapYear = false;
            if (year % 4 == 0) {
                if (year % 100 == 0) {
                    if (year % 400 == 0) {
                        isLeapYear = true;
                    }
                } else {
                    isLeapYear = true;
                }
            }

            // Print the result
            if (isLeapYear) {
                System.out.println(year + " is a leap year");
            } else {
                System.out.println(year + " is not a leap year");
            }

            //friend list

            ArrayList<String> friends = new ArrayList<String>();


            friends.add("John");
            friends.add("Jane");
            friends.add("Chloe");
            friends.add("Zoey");


            String message = "My friends are: " + friends;

            System.out.println(message);


            //Hashmap

            HashMap<String, Integer> inventory = new HashMap<String, Integer>();


            inventory.put("toothpaste", 15);
            inventory.put("toothbrush", 20);
            inventory.put("soap", 2);


            String HashMapmsg = "Current inventory consists of: " + inventory;

            // Print the result
            System.out.println(HashMapmsg);






        }
    }


