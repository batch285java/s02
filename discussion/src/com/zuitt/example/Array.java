package com.zuitt.example;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    public static void main(String[] args){
        //Java Collection
        //are a single unit of objects
        // useful for manipulating relevant pieces of data that can be used in different situations, more commonly with loops


        //Array Declaration

        //datatype[] identifier = new dataType[numOfElements]


        int[] intArray = new int[5];
        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 99;

        System.out.println(intArray);

        System.out.println(Arrays.toString(intArray));

        String[] names = {"John", "Jane", "Joe"};

        Arrays.sort(intArray);
        System.out.println("Order of items after sort();" + Arrays.toString(intArray));

        //Multidimensional Array
        // a two-dimensional array, can be described by two lengths nested within each other, like a matrix
        //first length is "row", second length is "column"

        String[][] classroom = new String[3][3];

        //First row
        classroom[0][0] = "Naruto";
        classroom[0][1] = "Sasuke";
        classroom[0][2] = "Sakura";

        //Second Row
        classroom[1][0] = "Linny";
        classroom[1][1] = "Tuck";
        classroom[1][2] = "Ming-Ming";

        //Third Row
        classroom[2][0] = "Harry";
        classroom[2][1] = "Ron";
        classroom[2][2] = "Hermione";

//        System.out.println(Arrays.toString(classroom));
        //for multi-dimensional arrays.
        System.out.println(Arrays.deepToString(classroom));

        //ArrayLists
            //are resizable arrays, wherein elements can be added or removed whenever it is needed
        //Syntax
            //ArrayList<T> identifier = new ArrayList<T>();
        //"<T>" is used to specify that the list can only have one type of objects in a collection
        //ArrayList cannot hold primitive data types, "java wrapper classes"


//        ArrayList<int> numbers = new ArrayList<int>();

       // ArrayList<Integer> numbers = new ArrayList<Integer>();

        // ArrayList<String> students = new ArrayList<String>();


        //add elements on Arraylist
        //arrayListName.add(element);
       // students.add("Cardo");
       // students.add("Luffy");
       // System.out.println(students);


        //Declare an ArrayList with values
        ArrayList<String> students = new ArrayList<>(Arrays.asList("Jane","Mike"));
        System.out.println(students);


        //access element
        //arrayListName.get(index);
        System.out.println(students.get(0));

    //add an element on specific index
        //ArrayListName.add(index, element);
        students.add(0,"CardiB");
        System.out.println(students.get(0));


        //updating an element
        //arrayListName.set(index, element)
        students.set(1, "Tom");
        System.out.println(students);

        //remove an element
        //arrayListName.remove(index)
        students.remove(1);
        System.out.println(students);

        //Remove all elements
        students.clear();
        System.out.println(students);

        //getting the arraylist size
        System.out.print(students.size());

        //HashMaps
          //most objects in Java are defined and instantiations of classes that contain a proper set of properties and methods.
        // there might be use cases where this is not appropriate, or you may simply want to store a collection of data in key-value pairs.

        //in Java "keys are also referred as "fields"
        //wherein the values are accessed by the fields
        //Syntax
        //Hashmap<dataTypeField, dataTypeValue> identifier = new Hashmap<>();

        HashMap<String,String> jobPosition = new HashMap<String,String>(){
            {
                put("Teacher", "Cee");
                put("Artist", "Michael Angelo");
            }
        };

        //hashMapName.put(<fieldName>, value)



        //declare hashmaps with initialization
        jobPosition.put("Dreamer", "Morpheus");
        jobPosition.put("Police", "Cardo");
        System.out.println(jobPosition);


        //access element
        //hashmapName.get("fieldName");
        System.out.println(jobPosition.get("Police"));


        //update the value
        //hashMapName.replace("fieldName", "NewValue");

        jobPosition.replace("Dreamer","Persephone");
        System.out.println(jobPosition);

        //remove an element
        jobPosition.remove("Police");
        System.out.println(jobPosition);

        //retrieve hashmap keys in an array
        System.out.println(jobPosition.keySet());
        System.out.println(jobPosition.values());


        //clear all elements in the hashmap
        //jobPosition.clear();
    }
}
