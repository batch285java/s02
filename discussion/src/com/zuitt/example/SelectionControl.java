package com.zuitt.example;

import java.util.Scanner;

public class SelectionControl {
    public static void main(String[] args){

        //Java Operators
        //arithmetic operators

        //Comparison
        // =, <, >, !=

        //Logical
        //&&, ||, !

        //Assignment
        // =

        //Mini Activity
        //create a divisibility by 5 checker using if and else

        int num = 36;

        if (num % 5 == 0){
            System.out.println(num + " is divisible by 5");
        }else{
            System.out.println(num +" is not divisible by 5");
        }



        //short-circuiting
        //a technique that is applicable only to the AND and OR operators wherein if statements or other control structures can exit early by ensuring safety of operation or efficiency


        int x = 15;
        int y = 0;

        if(y != 0 && x/y == 0){
            System.out.println("Result is: " + x/y);
        }else{
            System.out.println("This will only run because of short circuiting");
        }


        //Ternary Operator
        int num2 = 24;
        Boolean result =  (num2>0)?true:false;
        System.out.println(result);


        //Switch cases

        Scanner numberScanner = new Scanner(System.in);

        System.out.println("Enter a number");
        int directionValue = numberScanner.nextInt();

        switch (directionValue){

            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:
                System.out.println("Invalid");
        }


    }
}
